import React from 'react';
import { cleanup, render, fireEvent } from 'react-testing-library';

import App from './App';

beforeEach(cleanup);

test('submit form', () => {
    const { debug, getByText, getByPlaceholderText } = render(<App />);

    const emailField = getByPlaceholderText('Email');
    const passwordField = getByPlaceholderText('Password');
    const submitBtn = getByText('Log In');

    fireEvent.change(emailField, { target: { value: 'test@email.com' } });
    fireEvent.change(passwordField, { target: { value: 'testpw' } });
    fireEvent.click(submitBtn);
});
