import React from 'react';
import ReactDOM from 'react-dom';
import { submitFormMutation } from './formSubmission';

const LoginForm = ({ onSubmit }) => (
    <form onSubmit={onSubmit}>
        <input type="email" name="email" placeholder="Email" />
        <input type="password" name="password" placeholder="Password" />
        <button type="submit">Log In</button>
    </form>
);

const mutationFn = variables =>
    console.log('mutation function was called with variables', variables);

export default props => (
    <LoginForm onSubmit={submitFormMutation(mutationFn)} />
);
