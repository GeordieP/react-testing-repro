// add each field's values to accumulator by field's name attribute
// accumulator[field name] = field value.
const formReducer = (ac, field) => {
    if (!field) console.error('field is undefined'); // for debug!
    if (!field || !field.value || !field.name) return ac;

    const { type, name } = field;

    if (type === 'checkbox') ac[name] = field.checked;
    else ac[name] = field.value;

    return ac;
}

// return an object containing relevant form field names and their values;
// { field1: field1Value, field2: field2Value, ... }
export const submitForm = (e) => {
    e.preventDefault();

    // result here should be [input, input, button], as it is in a browser.
    // in a test, it's giving [undefined, undefined, undefined].
    console.log('target array is', Array.from(e.target));

    return Array.from(e.target)
                .reduce(formReducer, {});
}

// return a form onSubmit handler function that closes over the
// passed mutation callback fn, and calls that mutation when done.
export const submitFormMutation = (mutation) => (e) => {
    if (!mutation) return;

    const mutationVariables = submitForm(e);
    mutation(mutationVariables);
}
