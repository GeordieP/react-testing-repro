Reproduction for testing issue.

- `yarn` or `npm i` to install dependencies.

- `yarn start` or `npm run start` to build app and serve with Parcel, view in browser at `localhost:1234` (by default).

- `yarn test` or `npm run test` to run jest.
